<?php

session_start();
$name="";
$password="";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = check_input($_POST["user_id"]);
    $password = check_input($_POST["password"]);

    $_SESSION["name"] = $name;

    if($name == "jyotirmaya.s" && $password == "mindfire")
    {
        header("Location: /home.php");
    }
  }
  function check_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

    <link rel="stylesheet" href="css/login1.css">
    
</head>
<body class="bg-primary">
    <div class="lg_card bg-white" style="margin-top: 10%;">
        <div class="col-12 form_title">
            Login
        </div>
        <div class="mt-3">
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST" class="form-check">
                <div class="mb-3 px-5 ">
                    <input type="text" name="user_id" id="user_id" placeholder="user id">
                </div>
<?php

    if($_SERVER["REQUEST_METHOD"] == "POST" && $name != "jyotirmaya.s" )
    {
?>
               <div class="err_msg px-5"><sup>*</sup>Please enter a valid user id</div>
<?php
    }
?>
                <div class="mb-2 px-5">
                    <input type="password" name="password" id="password" placeholder="password">
                </div>
<?php

    if($_SERVER["REQUEST_METHOD"] == "POST" && $password != "mindfire" )
    {
?>
               <div class="err_msg px-5"><sup>*</sup>Please enter a valid password</div>
<?php
    }
?>
    
                <div class=" mb-5 text-right">
                    <a class="nav-link" href="">Forgot Password?</a>
                </div>
                <div class="px-5" style="margin-bottom: 2em;">
                    <input type="submit" value="submit" class="btn btn-primary btn-block btn_style">
                </div>
            </form>

        </div>
    </div>
</body>
</html>