<?php
    session_start();
    $target_dir = "uploads/";
    $target_file = $target_dir . basename($_FILES["img_upload"]["name"]);
    echo $target_file;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <link rel="stylesheet" href="css/edit-profile.css">
</head>
<body>
<?php require_once "header.php" ?>
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 col-md-4" style="border-right: 1px solid grey">
                <img src="image/img_avatar3.png" alt="profile image" class="rounded-circle">
                <br> <br>
                <form class="form" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-12 col-md-8">
                            <input type="file" name="img_upload" id="" class="form-control">
                        </div>
                        <div class="col-12 col-md-4">
                            <input type="submit" value="upload" class="btn btn-primary">    
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-12 col-md-8">
                <form action="#" method="post">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-6">
                                    <h4>Basic info</h4>
                                </div>
                                <div class="col-6 ">
                                    
                                    <button type="reset" class="btn btn-outline-danger float-md-right ">reset </button>
                                    <button type="submit" class="btn btn-primary float-md-right mr-2"> save</button>
                                </div>
                            </div>
                            <hr>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" id="name" class="form-control" placeholder="enter your name">
                                </div>
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" name="name" id="name" class="form-control" placeholder="enter your Title">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" id="email" class="form-control" placeholder="enter your email">
                                </div>
                                <div class="form-group">
                                    <label for="mb-no">mobile Number</label>
                                    <input type="text" name="mb-no" id="mb-no" maxlength="10" placeholder="0123456789" class="form-control">
                                </div>
                                <h4>About me</h4>
                                <hr >
                                <!-- age field -->
                                <div class="form-group">
                                    <label for="age">Age:</label>
                                    <input type="number" name="age" id="age" class="form-control">
                                </div>
                                <!-- select gender options -->
                                <div>
                                    <div class="form-check-inline">
                                        <label for="gender">Gender:</label>
                                    </div>
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="radio" name="gender" id="M" class="form-check-input"> Male  
                                        </label>
                                    </div>
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="radio" name="gender" id="F" class="form-check-input"> FeMale  
                                        </label>
                                    </div>
                                    <div class="form-check-inline">

                                        <label class="form-check-label">
                                            <input type="radio" name="gender" id="O" class="form-check-input"> others  
                                        </label>
                                    </div>
                                </div>
                                <!-- bio section -->
                                <div class="form-group">
                                    <label for="bio">biography</label>
                                    <textarea class="form-control" name="bio" id="bio" cols="30" rows="10"></textarea>
                                </div>

                                <!-- select skills section -->
                                <div>
                                    <label for="skills"> Skills:</label>
                                    <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="skills">HTML5
                                    </label>
                                    </div>
                                    <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="skills">CSS3
                                    </label>
                                    </div>
                                    <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="skills">Bootstrap4
                                    </label>
                                    </div>
                                    <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="skills">mysql
                                    </label>
                                    </div>
                                    <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="skills">javascript
                                    </label>
                                    </div>
                                </div>
                                <!-- address section  -->
                                <div>
                                    <div class="form-group">
                                        <label for="state">state:</label>
                                        <select name="state" id="state" class="form-control">
                                            <option value="odisha">odisha</option>
                                            <option value="ap">Andhra pradesh</option>
                                            <option value="delhi">delhi</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- upload resume section -->
                                <div>
                                    <label for="upload_resume">upload your resume:</label>
                                    <input type="file" name="resume" id="resume" class="form-control">
                                </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <br>
</section>
<?php require_once "footer.html"?>
</body>
</html>