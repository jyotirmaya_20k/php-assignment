<?php
session_start();

function redirect(){
    header("location: /profile.html");
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>DEMO site</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/home.css">
</head>
<body>

<?php
include_once "header.php"
?>
    <div class="home_body">
        <div class="row mt-5">
            <div class="col-12 col-md-4 pt-3">
                <img src="image/img_avatar3.png" alt="profile image" class="rounded-circle">
            </div>
            <div class="col-12 col-md-8 display-4 ">
                hey! <?php echo $_SESSION["name"]  ?>
                welcome to DEMO site..
                <br>
                <a href="/edit-profile.php" class="btn btn-outline-primary" > complete your profile </a>
            </div>
        </div>
    </div>
<?php
include_once "footer.html"
?>
</body>
</html>
