
<nav class="navbar navbar-expand-md bg-dark navbar-dark fixed-top">
    <a href="/" class="navbar-brand">DEMO site</a>

    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar_items">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbar_items" style="margin-left: 30%;">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="" class="nav-link">my profile</a>
            </li>
            <li class="nav-item">
                <a href="" class="nav-link">about us</a>
            </li>
            <li class="nav-item">
                <a href="" class="nav-link">contact us</a>
            </li>
        </ul>
    </div>
</nav>
